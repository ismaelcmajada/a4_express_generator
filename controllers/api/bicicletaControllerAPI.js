
let Bicicleta = require("../../models/Bicicleta");

exports.bicicleta_list = function(req, res) {

    Bicicleta.allBicis(function (err, bicis) {
        if (err) res.send(500, err.message);

        res.status(200).json ({

            bicicletas: bicis
    
        });
       
      })
    
};

exports.bicicleta_create = function(req, res) {

    let bici = new Bicicleta({
        bicicletaID: req.body.bicicletaID,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion:
          [req.body.latitud, req.body.longitud]
      });
    
      Bicicleta.add(bici, function (err,newBici) {
    
        if (err) res.send(500, err.message);
    
        res.status(201).json({
            bicicleta: newBici
        });
      });
  }

exports.bicicleta_delete = function(req, res) {

    Bicicleta.removeById(req.body.bicicletaID, function (err) {
        if (err) res.send(500, err.message);
        res.status(204).send();
      });

    
}

exports.bicicleta_update = function(req, res) {

    Bicicleta.findById(req.params.bicicletaID, function (err, bici) {
        if (err) res.send(500, err.message);
    
        bici.bicicletaID = req.body.bicicletaID;
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.latitud, req.body.longitud];
    
        bici.save();
    
        res.status(200).json({
            bicicleta: bici
        });
    });
  
}