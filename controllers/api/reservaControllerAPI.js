let Reserva = require("../../models/Reserva");

exports.reserva_list = function(req, res) {

    Reserva.allReservas(function (err, reservas) {
        if (err) res.send(500, err.message);

        res.status(200).json ({

            reservas: reservas
    
        });
       
      })
    
}

exports.reserva_delete = function(req, res) {

    Reserva.removeById(req.body._id, function (err) {
        if (err) res.send(500, err.message);
        res.status(204).send();
      });

    
}

exports.reserva_update = function(req, res) {

    Reserva.findById(req.params._id, function (err, reserva) {
        if (err) res.send(500, err.message);
    
        reserva.desde = req.body.desde;
   
        reserva.hasta = req.body.hasta;
   
        reserva.bicicleta = req.body.bicicleta;
   
        reserva.usuario = req.body.usuario; 
    
        reserva.save();
    
        res.status(200).json({
            reserva: reserva
        });
    });
}