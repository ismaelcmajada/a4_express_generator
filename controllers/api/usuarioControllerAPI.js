let Usuario = require("../../models/Usuario");

exports.usuario_list = function(req, res) {

    Usuario.allUsuarios(function (err, usus) {
        if (err) res.send(500, err.message);

        res.status(200).json ({

            usuarios: usus
    
        });
       
      })
    
}

exports.usuario_create = function(req, res) {

    let usuario = new Usuario({
        nombre: req.body.nombre,
      });
    
      Usuario.add(usuario, function (err,newUsuario) {
    
        if (err) res.send(500, err.message);
    
        res.status(201).json({
            usuario: newUsuario
        });
      });
  }

exports.usuario_delete = function(req, res) {

    Usuario.removeById(req.body._id, function (err) {
        if (err) res.send(500, err.message);
        res.status(204).send();
      });

    
}

exports.usuario_update = function(req, res) {

    Usuario.findById(req.params._id, function (err, usu) {
        if (err) res.send(500, err.message);
    
        usu.nombre = req.body.nombre;
    
        usu.save();
    
        res.status(200).json({
            usuario: usu
        });
    });
}

exports.usuario_reservar = function(req,res){ 

    Usuario.findById(req.body._id, function(err,usu) {
   
    if(err) res.status(500).send(err.message);
   
    console.log(usu);
   
    usu.reservar(req.body.bicicleta, req.body.desde, req.body.hasta, function(err){
   
    console.log("Reservada!!");
   
    res.status(200).send();
   
    });
   
    });
}