let Bicicleta = require("../models/Bicicleta");

exports.bicicleta_list = function (req, res) {
  Bicicleta.allBicis(function (err, bicis) {
    if (err) res.send(500, err.message);
    res.render("bicicletas/index", { bicis });
  })

}

exports.bicicleta_create_get = function (req, res) {
  res.render("bicicletas/create");
}

exports.bicicleta_create_post = function (req, res) {

  let bici = new Bicicleta({
    bicicletaID: req.body.bicicletaID,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion:
      [req.body.latitud, req.body.longitud]
  });

  Bicicleta.add(bici, function (err) {

    if (err) res.send(500, err.message);

    res.redirect("/bicicletas");
  });
}

exports.bicicleta_delete_post = function (req, res) {

  let bici = req.body.bicicletaID;

  Bicicleta.removeById(bici, function (err) {
    if (err) res.send(500, err.message);
  });

  res.redirect("/bicicletas");
}

exports.bicicleta_update_get = function (req, res) {

  Bicicleta.findById(req.params.bicicletaID, function (err, bici) {
    if (err) res.send(500, err.message);
    res.render("bicicletas/update", { bici });

  });

}

exports.bicicleta_update_post = function (req, res) {

  Bicicleta.findById(req.params.bicicletaID, function (err, bici) {
    if (err) res.send(500, err.message);

    bici.bicicletaID = req.body.bicicletaID;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.latitud, req.body.longitud];

    bici.save();

    res.redirect("/bicicletas");

  });

}