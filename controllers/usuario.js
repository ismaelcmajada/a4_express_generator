let Usuario = require("../models/Usuario");

module.exports = {
    list: function(req,res,next){   //Devuelve el listado de usuarios
     Usuario.find({}, function(err,usuarios) {
            if(err) res.send(500, err.message);
            res.render("usuarios/index", {usuarios: usuarios}); //Introduce la lista de usuarios en la vista.
        });
    },
    update_get: function(req,res, next) { //Abre la vista de update del usuario pasado por parámetro.
        Usuario.findById(req.params.id, function (err, usuario) {
            res.render("usuarios/update", {errors:{}, usuario: usuario}); //Se cargan los datos del usuario en el formulario de la vista
        });
    },
    update: function(req, res, next){ //Este método se carga al enviar el formulario de actualización.
        let update_values = {nombre: req.body.nombre};
        Usuario.findByIdAndUpdate(req.params.id, update_values, function(err, usuario) { //Actualiza los datos del usuario pasado por parámetro, usando los datos del formulario.
            if (err) {
                console.log(err);
                res.render("usuario/update", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})}); //En caso de error vuelve a la vista y muestra el error.
            } else {
                res.redirect("/usuarios"); //En caso contrario vuelve a /usuarios
                return;
            }
        });
    },
    create_get: function(req, res, next){ //Carga la vista para crear usuarios
        res.render("usuarios/create", {errors:{}, usuario: new Usuario()});
    },
    create: function(req, res, next) { //Este método se carga al recibir los datos del formulario por post
        if (req.body.password != req.body.confirm_password) { //En caso de que las contraseñas no sean iguales, se vuelve a la vista, manteniendo los datos del formulario y se muestra el error.
            res.render("usuarios/create", {errors: {confirm_password: {message: "No coincide con el password introducido."}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            return;
        }
        Usuario.create({nombre: req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoUsario) { //Vamos a crear el nuevo usuario
            if(err){ //En caso de error, se vuelve a cargar la vista, mostrando los errores y manteniendo los datos del usuario.
                res.render("usuarios/create", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            } else { //En caso contrario, se envía el email de bienvenida y se vuelve a /usuarios
                nuevoUsario.enviar_email_bienvenida();
                res.redirect("/usuarios");
            }
        });
    },
    delete: function(req,res,next){ //Este método se encarga de borrar un usuario
        Usuario.findByIdAndDelete(req.body.id, function(err){ //Elimina el usuario con el id pasado por formulario.
            if(err)
                next(err); //En caso de error lo devuelve
            else   
                res.redirect("/usuarios"); //En caso contrario vuelve a /usuarios
        });
    }
};