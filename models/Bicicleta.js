// Atributos
let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let Bicicleta = function (bicicletaID, color, modelo, ubicacion)  {
  this.bicicletaID = bicicletaID;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion
}

let bicicletaSchema = new mongoose.Schema({

  bicicletaID: Number,

  color: String,

  modelo: String,

  ubicacion: { type: [Number], index: true }

});

bicicletaSchema.statics.allBicis = function (cb) {
  return this.find({}, cb);
}

bicicletaSchema.statics.add = function (aBici, cb) {
  return this.create(aBici, cb);
}

bicicletaSchema.statics.findById = function (biciID, cb) {
  return this.findOne({bicicletaID: biciID}, cb);
}

bicicletaSchema.statics.removeById = function (biciID, cb) {
  return this.deleteOne({bicicletaID: biciID}, cb);
}


module.exports = mongoose.model("Bicicleta", bicicletaSchema);