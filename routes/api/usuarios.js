let express = require('express');

let router = express.Router();

let usuarioControllerAPI= require("../../controllers/api/usuarioControllerAPI");

router.get("/", usuarioControllerAPI.usuario_list);
router.post("/create", usuarioControllerAPI.usuario_create);
router.delete("/delete", usuarioControllerAPI.usuario_delete);
router.put("/update/:_id", usuarioControllerAPI.usuario_update);
router.post("/reservar", usuarioControllerAPI.usuario_reservar);

module.exports = router;