let Bicicleta = require("../../models/Bicicleta");
const request = require('request');
let server = require("../../bin/www");

beforeEach (() => {Bicicleta.allBicis=[]});

// Test de la API

describe('Test de la API', () => {
    describe('GET /', () => {
        
        it('Devuelve status 200', () => {

            expect(Bicicleta.allBicis.length).toBe(0);

            let a = new Bicicleta(1, "azul", "xiaomi", [28.503789, -13.853296]);

            Bicicleta.add(a);

            request.get("http://localhost:3000/api/bicicletas", (error,response,body) => {
                expect(response.statusCode).toBe(200);
            });

        });

    });
});

// Test del create

describe('POST /create', () => {
    it('Devuelve status 201', (done) => {

        let headers = {"Content-type": "application/JSON"};

        let aBici = '{"id": 10, "color": "azul", "modelo": "xiaomi", "lat": "28.503789", "long": "-13.853296"}';

        request.post({
            
            headers: headers,

            url: "http://localhost:3000/api/bicicletas/create",

            body: aBici

        }, (error,response,body) => {

            expect(response.statusCode).toBe(201);

            expect(Bicicleta.findById(10).color).toBe("azul");

            done();
        
        });
        
    });
});

//Test del delete

describe('DELETE /delete', () => {
    it('Devuelve status 204', (done) => {

        let a = new Bicicleta(10, "Azul", "Urbana", [28.503789, -13.853296]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);

        let headers = {"Content-type": "application/JSON"};

        let aBici = '{"id": 10}';

        request.delete({
            
            headers: headers,

            url: "http://localhost:3000/api/bicicletas/delete",

            body: aBici

        }, (error,response,body) => {

            expect(response.statusCode).toBe(204);

            expect(Bicicleta.allBicis.length).toBe(0);

            done();
        
        });
        
    });
});

// Test del update

describe('UPDATE /update/:id', () => {
    it('Devuelve status 200', (done) => {

        let a = new Bicicleta(10, "Azul", "Urbana", [28.503789, -13.853296]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);

        let headers = {"Content-type": "application/JSON"};

        let aBici = '{"id": 9, "color": "rojo", "modelo": "xiaomi", "lat": "28.500000", "long": "-13.850000"}';

        request.put({
            
            headers: headers,

            url: "http://localhost:3000/api/bicicletas/update/10",

            body: aBici

        }, (error,response,body) => {

            expect(response.statusCode).toBe(200);

            expect(Bicicleta.findById(9).color).toBe("rojo");
            expect(Bicicleta.findById(9).modelo).toBe("xiaomi");

            done();
        
        });
        
    });
});