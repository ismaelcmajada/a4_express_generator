let mongoose = require("mongoose");

let Bicicleta = require("../../models/Bicicleta");

describe("Testing unitario Bicicletas", function () {

    beforeEach(function (done) {

        var mongoDB = "mongodb://localhost/testdb";

        mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true });

        var db = mongoose.connection;

        db.on("error", console.error.bind('Error de conexión con MongoDB'));

        db.once("open", function () {

            console.log("Conectado a la BBDD testdb");

            done();

        });

    });



    afterEach(function (done) {

        Bicicleta.deleteMany({}, function (err, success) {

            if (err) console.log(err);

            done();

        });

    });




    describe("Bicicleta.allBicis", () => {

        it("Empieza vacío", (done) => {

            Bicicleta.allBicis(function (err, bicis) {

                expect(bicis.length).toBe(0);

                done();

            });

        });

    });


    // Test del add

    describe("Bicicleta.add", () => {

        it("Agrega una bici", (done) => {

            let aBici = new Bicicleta({ bicicletaID: 20, color: "azul", modelo: "urbana" });

            Bicicleta.add(aBici, (err) => {

                if (err) console.log(err);

                Bicicleta.allBicis((err, bicis) => {

                    expect(bicis.length).toEqual(1);

                    expect(bicis[0].bicicletaID).toEqual(aBici.bicicletaID);

                    done();

                });

            });

        });

    });

    // Test del findById

    describe("Bicicleta.findById", () => {
        it("Debe devolver la bici con id 20", (done) => {

            let aBici = new Bicicleta({ bicicletaID: 20, color: "azul", modelo: "urbana" });

            Bicicleta.add(aBici, (err,bici) => {

                if (err) console.log(err);

                Bicicleta.findById(20, (err, targetBici) => {

                    expect(targetBici.bicicletaID).toBe(20);
                    expect(targetBici.color).toBe(bici.color);
                    expect(targetBici.modelo).toBe(bici.modelo);
    
                    done();
    
                });

            });

        });
    });

    // Test del removeById

    describe("Bicicleta.removeById", () => {
        it("Debe eliminar la bici con id 20", (done) => {

            let aBici = new Bicicleta({ bicicletaID: 20, color: "azul", modelo: "urbana" });

            Bicicleta.add(aBici, (err) => {

                if (err) console.log(err);

                Bicicleta.removeById(aBici.bicicletaID, (err)=>{

                    if (err) console.log(err);
    
                    Bicicleta.allBicis((err, bicis) => {
    
                        if (err) console.log(err);
        
                        expect(bicis.length).toEqual(0);
                        done(); 
        
                    });
        
                });
            }); 

        });
    });
});
